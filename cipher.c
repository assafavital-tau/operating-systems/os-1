#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_LIMIT 4096
#define FILENAME_LENGTH 1024

int xorfile(const char* srcfile, const char* keyfile, int fd, const char* dstdir, const char* dstfilename)
{
	int i, j = 0;
	int src_fd, dst_fd;
	int key_bytes, src_bytes;
	char dst_filename[BUFFER_LIMIT];
	char key_buffer[BUFFER_LIMIT], src_buffer[BUFFER_LIMIT];

	// Open input file
	if((src_fd = open(srcfile, O_RDONLY)) < 0)
	{
		printf("Could not open input file.");
		return 1;
	}

	// Open output file
	sprintf(dst_filename, "%s/%s", dstdir, dstfilename);
	if((dst_fd = creat(dst_filename, O_RDWR | O_CREAT | O_TRUNC | 0777)) < 0)
	{
		printf("Could not open output file.");
		close(src_fd);
		return 1;
	}

	// Read key file
	if((key_bytes = read(fd, key_buffer, BUFFER_LIMIT)) < 0)
	{
		printf("Could not read key file.");
		close(src_fd);
		close(dst_fd);
		return 1;
	}

	do
	{
		// Read from source
		if((src_bytes = read(src_fd, src_buffer, BUFFER_LIMIT)) < 0)
		{
			printf("Could not read input file.");
			close(src_fd);
			close(dst_fd);
			return 1;
		}

		// Execute bitwise xor
		for(i = 0; i < src_bytes; i++)
		{
			src_buffer[i] = (char)(src_buffer[i] ^ key_buffer[j]);

			// Prevent overflow in key file
			if(j >= key_bytes - 1)
			{
				// Re-open key file
				if(close(fd) < 0)
				{
					printf("Failed to close key file.");
					close(src_fd);
					close(dst_fd);
					return 1;
				}
				if((fd = open(keyfile, O_RDONLY)) < 0)
				{
					printf("Could not open key file.");
					close(src_fd);
					close(dst_fd);
					return 1;
				}

				// Read from key file
				if((key_bytes = read(fd, key_buffer, BUFFER_LIMIT)) < 0)
				{
					printf("Could not read key file.");
					close(src_fd);
					close(dst_fd);
					return 1;
				}
				j = 0;
			}
			else
				j++;
		}

		// Write to output file
		if(write(dst_fd, src_buffer, src_bytes) < 0)
		{
			printf("Failed to write to output file.");
			close(src_fd);
			close(dst_fd);
			return 1;
		}
	}
	while(src_bytes >= BUFFER_LIMIT);

	// Close resources
	if(close(src_fd) < 0)
	{
		printf("Failed to close input file.");
		close(dst_fd);
		return 1;
	}
	if(close(dst_fd) < 0)
	{
		printf("Failed to close output file.");
		return 1;
	}

	return 0;
}

int main(int argc, char** argv)
{
	// Check for correct number of arguments
	if(argc != 4)
	{
		printf("Invalid command-line arguments.");
		return 1;
	}

	// Assign arguments
	char* src_dir = argv[1];
	char* dst_dir = argv[3];
	char* key_file = argv[2];

	// Check for valid source directory
	DIR* src = opendir(src_dir);
	if(!src)
	{
		printf("Input directory does not exist.");
		return 1;
	}

	// Check for valid key file
	int fd = open(key_file, O_RDONLY);
	if(fd < 0)
	{
		printf("Key file does not exist.");
		closedir(src);
		return 1;
	}

	// Check for valid destination directory
	struct stat64 stdir;
	if(stat64(dst_dir, &stdir) < 0)
	{
		// Create folder
		if(mkdir(dst_dir, 0777) < 0)
		{
			printf("Failed to create destination directory.");
			close(fd);
			closedir(src);
			return 1;
		}
	}
	DIR* dst = opendir(src_dir);
	if(!src)
	{
		printf("Failed to open destination directory.");
		close(fd);
		closedir(src);
		closedir(dst);
		return 1;
	}

	// Iterate over input files
	struct dirent* dent;
	char filename[FILENAME_LENGTH];
	struct stat64 filest;
	while((dent = readdir(src)))
	{
		if(strcmp(dent->d_name,".") == 0 || strcmp(dent->d_name,"..") == 0)
			continue;
		// Get new file name
		sprintf(filename, "%s/%s", src_dir, dent->d_name);

		// Get file stat
		if(stat64(filename, &filest) < 0)
		{
			printf("Failed to get stat for input file.");
			close(fd);
			closedir(src);
			closedir(dst);
			return 1;
		}

		// Encrypt/Decrypt file
		if(xorfile(filename, key_file, fd, dst_dir, dent->d_name) != 0)
		{
			close(fd);
			closedir(src);
			closedir(dst);
			return 1;
		}
	}

	// Close resources
	if(close(fd) < 0)
	{
		printf("Failed to close key file.");
		closedir(src);
		closedir(dst);
		return 1;
	}
	if(closedir(src) < 0)
	{
		printf("Failed to close source directory.");
		closedir(dst);
		return 1;
	}
	if(closedir(dst) < 0)
	{
		printf("Failed to close destination folder.");
		return 1;
	}



	return 0;
}
